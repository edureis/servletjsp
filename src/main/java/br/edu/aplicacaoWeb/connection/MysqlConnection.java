package br.edu.aplicacaoWeb.connection;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.ResourceBundle;

import br.edu.aplicacaoWeb.util.Constantes;

public class MysqlConnection {
	
	private static ResourceBundle configBD = ResourceBundle.getBundle(Constantes.CONNECTION_BD_PROPERTIES);
	
	private String driver = configBD.getString(Constantes.CONNECTION_BD_DRIVER);
	private String url = configBD.getString(Constantes.CONNECTION_BD_URL);
	private String user = configBD.getString(Constantes.CONNECTION_BD_USER);
	private String password = configBD.getString(Constantes.CONNECTION_BD_PASSWORD);
	
		
	private static MysqlConnection connection;
	
	public static MysqlConnection getInstance() {
		if(connection == null) {
			connection = new MysqlConnection();
		}
		return connection;
	}
		
	public Connection getConnection() {
		Connection connection = null;
		try {
			Class.forName(driver);
		} catch (ClassNotFoundException e) {
			System.out.println("Driver não encontrado");
			e.printStackTrace();
		}
		try {
			connection = DriverManager.getConnection(url, user, password);
		} catch (SQLException e) {
			System.out.println("Erro na String de conexão!");
			e.printStackTrace();
		}
		return connection;
	}
	
	public static void main(String[] args) {
		System.out.println(getInstance().getConnection());
	}
	
}

