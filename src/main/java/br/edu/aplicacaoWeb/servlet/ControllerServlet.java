package br.edu.aplicacaoWeb.servlet;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import br.edu.aplicacaoWeb.mvc.logica.Logica;



@WebServlet("/controller")
public class ControllerServlet extends HttpServlet{

	private static final long serialVersionUID = 1L;
	
	@Override
	protected void service(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String parametro = request.getParameter("logica");
		String nomeDaClasse = "br.edu.aplicacaoWeb.mvc.logica." + parametro;
		
		try {
			Class classe = Class.forName(nomeDaClasse);
			
			Logica logica = (Logica) classe.newInstance();
			String pagina = logica.execute(request, response);
			
			request.getRequestDispatcher(pagina).forward(request, response);
			
		} catch (Exception e) {			
			e.printStackTrace();
		}
		
	}

}
