package br.edu.aplicacaoWeb.util;

public class Constantes {
	
	public static final String CONNECTION_BD_PROPERTIES = "br.edu.aplicacaoWeb.util.config_bd";
	
	public static final String CONNECTION_BD_DRIVER = "connection.driver.mysql";
	
	public static final String CONNECTION_BD_URL = "connection.url";
	
	public static final String CONNECTION_BD_USER = "connection.user";
	
	public static final String CONNECTION_BD_PASSWORD = "connection.password";
}
