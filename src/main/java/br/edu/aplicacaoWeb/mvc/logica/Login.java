package br.edu.aplicacaoWeb.mvc.logica;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import br.edu.aplicacaoWeb.bo.LoginBO;
import br.edu.aplicacaoWeb.exception.NegocioException;
import br.edu.aplicacaoWeb.model.Usuario;

public class Login implements Logica{

	@Override
	public String execute(HttpServletRequest request, HttpServletResponse response) {
		String pagina = "login.jsp";
		LoginBO loginBO = new LoginBO(); 
		
		String login = request.getParameter("login");
		String senha = request.getParameter("senha");
		
		Usuario usuario = new Usuario();
		usuario.setUsuario(login);
		usuario.setSenha(senha);
		
		try {
			if(loginBO.validaUsuario(usuario)) {
				pagina = "index.jsp";
			}
		} catch (NegocioException e) {			
			e.printStackTrace();
			request.setAttribute("msgErro", e.getMessage());
		}
		
		return pagina;
	}

}
