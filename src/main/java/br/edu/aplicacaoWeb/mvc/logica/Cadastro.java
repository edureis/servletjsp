package br.edu.aplicacaoWeb.mvc.logica;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class Cadastro implements Logica{

	@Override
	public String execute(HttpServletRequest request, HttpServletResponse response) {		
		return "cadastro.jsp";
	}

}
