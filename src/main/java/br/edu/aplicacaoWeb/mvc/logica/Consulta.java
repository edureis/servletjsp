package br.edu.aplicacaoWeb.mvc.logica;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class Consulta implements Logica{

	@Override
	public String execute(HttpServletRequest request, HttpServletResponse response) {		
		return "consulta.jsp";
	}

}
