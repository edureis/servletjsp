package br.edu.aplicacaoWeb.bo;

import java.util.HashMap;
import java.util.Map;

import br.edu.aplicacaoWeb.dao.UsuarioDAO;
import br.edu.aplicacaoWeb.exception.NegocioException;
import br.edu.aplicacaoWeb.exception.ValidationException;
import br.edu.aplicacaoWeb.model.Usuario;
import br.edu.aplicacaoWeb.validator.LoginValidator;
import br.edu.aplicacaoWeb.validator.Validator;

public class LoginBO {
	
	public boolean validaUsuario(Usuario usuario) throws NegocioException{
		boolean isValido = false;
		Map<String, Object> valores = new HashMap<>();
		valores.put("Usuário", usuario.getUsuario());
		valores.put("Senha", usuario.getSenha());
		
		Validator loginObrigatorioValidator = new LoginValidator();
		
		try {
			if(loginObrigatorioValidator.validaCampos(valores)) {
				UsuarioDAO dao = new UsuarioDAO();
				isValido = dao.validaUsuario(usuario);			
			}
		} catch (Exception e) {
			throw new NegocioException(e);
		}
		return isValido;
		
	}
	
}
