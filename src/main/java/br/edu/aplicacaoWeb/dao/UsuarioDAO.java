package br.edu.aplicacaoWeb.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import br.edu.aplicacaoWeb.connection.MysqlConnection;
import br.edu.aplicacaoWeb.exception.PersistenceException;
import br.edu.aplicacaoWeb.model.Usuario;

public class UsuarioDAO {
	public boolean validaUsuario(Usuario usuario) throws PersistenceException{
		boolean retorno = false;
		try(Connection connection = MysqlConnection.getInstance().getConnection()) {
			StringBuilder sql = new StringBuilder();
			sql.append("SELECT * FROM tb_usuario");
			sql.append(" WHERE usuario = ? AND senha = ?");
			
			PreparedStatement stmt = connection.prepareStatement(sql.toString());
			stmt.setString(1, usuario.getUsuario());
			stmt.setString(2, usuario.getSenha());
			
			ResultSet rs = stmt.executeQuery();
			retorno = rs.next();
			stmt.close();
			rs.close();
			
		} catch (Exception e) {
			throw new PersistenceException("Erro no script SQL!", e);
		}
		return retorno;
	}	
}
