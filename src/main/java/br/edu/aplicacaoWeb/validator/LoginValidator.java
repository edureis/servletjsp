package br.edu.aplicacaoWeb.validator;

import java.util.Map;

import br.edu.aplicacaoWeb.exception.ValidationException;
import br.edu.aplicacaoWeb.message.Messages;

public class LoginValidator implements Validator{

	@Override
	public boolean validaCampos(Map<String, Object> valores) throws ValidationException {
		String msgErro = "";
		for(String key : valores.keySet()) {
			String campo = (String) valores.get(key);
			if(campo == null || "".equals(campo)) {
				msgErro += Messages.MSG_ERRO_CAMPO_OBRIGATORIO.replace("?", key).concat("<br/>");
			}
		}
		if(!"".equals(msgErro)) {
			throw new ValidationException(msgErro);
		}
		return true;
	}

}
