package br.edu.aplicacaoWeb.validator;

import java.util.Map;

import br.edu.aplicacaoWeb.exception.ValidationException;

public interface Validator {
	
	boolean validaCampos(Map<String, Object> valores) throws ValidationException;
}
