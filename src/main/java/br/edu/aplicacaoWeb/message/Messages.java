package br.edu.aplicacaoWeb.message;

public class Messages {
	
	public static final String MSG_ERRO_CAMPO_OBRIGATORIO = "Campo ? obrigatório!";
	
	public static final String MSG_ERRO_CAMPO_INVALIDO = "Campo ? inválido!";
}
